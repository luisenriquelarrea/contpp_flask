from app.db import db, BaseModelMixin


class Empleado(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    nombre_completo = db.Column(db.String)
    rfc = db.Column(db.String)
    curp = db.Column(db.String)

    def __init__(self, nombre_completo, rfc, curp):
        self.nombre_completo = nombre_completo
        self.rfc = rfc
        self.curp = curp

    def __repr__(self):
        return f'Empleado({self.nombre_completo})'

    def __str__(self):
        return f'{self.nombre_completo}'