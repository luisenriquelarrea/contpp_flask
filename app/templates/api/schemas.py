from marshmallow import fields

from app.ext import ma


class EmpleadoSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    nombre_completo = fields.String()
    rfc = fields.String()
    curp = fields.String()