from flask import request, Blueprint
from flask_restful import Api, Resource

from .schemas import EmpleadoSchema
from ..models import Empleado

empleado_bp = Blueprint('empleado_bp', __name__)

empleado_schema = EmpleadoSchema()

api = Api(empleado_bp)

class EmpleadoListResource(Resource):
    def get(self):
        empleados = Empleado.get_all()
        result = empleado_schema.dump(empleados, many=True)
        return result
    
    def post(self):
        data = request.get_json()
        empleado_dict = empleado_schema.load(data)
        empleado = Empleado(nombre_completo=empleado_dict['nombre_completo'],
                    rfc=empleado_dict['rfc'],
                    curp=empleado_dict['curp']
        )
        empleado.save()
        resp = empleado_schema.dump(empleado)
        return resp, 201

class EmpleadoResource(Resource):
    def get(self, empleado_id):
        empleado = Empleado.get_by_id(empleado_id)
        if empleado is None:
            raise ObjectNotFound('La película no existe')
        resp = empleado_schema.dump(empleado)
        return resp

api.add_resource(EmpleadoListResource, '/api/empleado/', endpoint='empleado_list_resource')
api.add_resource(EmpleadoResource, '/api/empleado/<int:empleado_id>', endpoint='empleado_resource')